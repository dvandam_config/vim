# Fork differences

This fork changes the following

* Uses the system clipboard instead of the vim clipboard
* Two spaces for BibTeX and LaTeX files

# Supported versions

Both vim and neovim are supported. The vimrc is tested with the latest package
available at [Arch Linux](https://www.archlinux.org/).

# First time use

Note that this configuration focuses on C, C++, ReST, PlantUML and LaTeX.

## Dependencies

* Install [ctags](http://ctags.sourceforge.net/)
* Install [Vim-latexSuite](http://vim-latex.sourceforge.net/).
* Install vim-spell-en and vim-spell-nl.
  * **NOTE:** Neovim users should check the bug list.
* Install clang and clang-tools-extra (clangd, clang-{format,tidy}).
  * For advanced C/C++ code checking.
* Install pynvim (python3-neovim), even for regular vim.
  * For completion framework.
* Install rust (including cargo).
  * For compiling LanguageClient-neovim, even for regular vim.

# Install

Clone the repo, cd into it and run the following commands:

```
ln -s "$PWD/vimrc" ~/.vimrc
mkdir -p ~/.vim
ln -s "$PWD/colors" ~/.vim/colors
```

Then start up vim, you may get a few errors due to plugins not yet being
installed. Vim-plug will then install plugins, restart vim afterwards.

* `:PlugUpgrade` to upgrade Vim-plug.
* `:PlugUpdate` to update existing plugins.
* `:PlugInstall` to install missing plugins.
* `:PlugClean` to remove old plugins.

# Bugs

**Neovim:**

* [Spelling checker](https://github.com/neovim/neovim/issues/1551) : When using
  neovim for the first time, comment the `set spell` command in the vimrc. Then
  start up neovim and `set spell` for both the EN and NL spell packages. This
  will manually download the spell packages. When the download is finished,
  uncomment the `set spell` command, and have fun!

# Features

Several features are included in the vimrc file. These will be described in this
section. Note that the default leader is both `\` and `SPACE`.

## Deletion without saving

Normally when you delete characters in vim, it will be saved in the register.
With these shortcuts it won't be saved.  Before the normal deletion characters
press the leader key followed by `o`.  So as example, if you want to delete a
line without saving press `<leader>odd`.

## Character limit

For all files a character limit is set at 80 characters. There are two different
modes to show this. One that highlights all the characters over the character
limit and another one that shows a vertical line on the character limit. This
mode can be toggled with `<leader>cl`, standing for **c**haracter **l**imit.

Vim will automatically wrap when typing. Make a selection and use `gq` to format
a section manually or run `gqip` directly to format the current inner paragraph.
To disable this temporarily use `set tw=0`.

## Trailing spaces deletion

When a file is saved, all trailing spaces will be deleted.

## File types

Some additional filetypes are set for several extensions. Notably `.h` files are
now set to C instead of C++.

## Persistent undo

The persistent undo feature is enabled if vim was compiled with support for it.
Undo information is stored in the `~/.vim/undo` directory.

## Manual page order

The `man` page search order has been changed to prefer POSIX pages.

## Default window management

Any preview window that is currently opened can be closed with `<leader>cf`.

# Plugins

A short introduction for all the plugins used with this vimrc.

Vim-plug is used as the plugin manager. The vimrc includes a section to
automatically download vim-plug if it isn't installed.

## [vim-table-mode](https://github.com/dhruvasagar/vim-table-mode/)

Align tables automatically.

**Few shortcuts**

`<leader>tm` toggles table mode.

`<leader>tt` formats existing (CSV) data into a table.

## [syntastic](https://github.com/scrooloose/syntastic)

Syntax checker.

**Few shortcuts**

`<leader>c` compiles the current file.

`<leader>co` opens the location list with errors.

`<leader>cd` closes the location list with errors.

## [nerdtree](https://github.com/scrooloose/nerdtree)

Filesystem explorer. Opens by default if vim is started without any arguments.

**Few shortcuts**

`<C-t>`, `<F3>` toggles nerdtree.

`<leader>tn` opens a new tab with nerdtree opened.

## [tagbar](http://ctags.sourceforge.net/)

Browse the tags of the current file and get an overview of its structure.

**Few shortcuts**

`<leader>t` toggles the tag sidebar.

## [ctrlp](https://github.com/ctrlpvim/ctrlp.vim)

Full path fuzzy file, buffer, mru and tag finder.

**Few shortcuts**

`<C-p>` Toggle CtrlP modus.

`<C-f>` or `<C-b>`Cycle between modes (when CtrP is open).

`<C-t>`, `<C-v>` or `<C-x`> Open selected entry in a new tab or in new split
(when CtrlP is open).

## [airline](https://github.com/vim-airline/vim-airline)

Lean & mean status/tabline for vim that's light as air.

**Few shortcuts (normal mode)**

`<leader>{1-9}`: Switch to buffer {1-9}.

`<leader>bp`, `<S-Tab>`: Switch to previous buffer tab.

`<leader>bn`, `<Tab>`: Switch to next buffer tab.

## [latex](http://vim-latex.sourceforge.net/)

Use LaTeX in vim.

**Few shortcuts**

`<leader>ll` compiles the document.

`<leader>lv` views the document.

`<leader>lo` start live previewing.

## Spell checking
Checks the spelling of the file.

**Few shortcuts**

`<leader>ss` toggle spell checking.

`<leader>sl` cycle through spell languages.

`<leader>sn` or `]s` next misspelled word.

`<leader>sp` or `[s` pevious mispelled word.

`<leader>sa` or `zg` add word to dictionary.

`<leader>s?` or `z=` list alternative words.

## [vim-clang-format](https://github.com/rhysd/vim-clang-format)

Formats code using clang-format on insert mode leave and buffer write.

Only enabled when `.clang-format` can be found, does nothing otherwise.

## [DoxygenToolkit.vim](https://vim.sourceforge.io/scripts/script.php?script_id=987)

Generate Doxygen template for the function, preconfigured to use `\brief` style.

**Few shortcuts**

`<leader>ENTER` generate Doxygen template for function on cursor position.

## [vim-vebugger](https://github.com/idanarye/vim-vebugger)

Debugger frontend. Supports several debuggers for several languages.

Requires the [vimproc](https://github.com/Shougo/vimproc.vim) plugin which is
also included in this config. Vimproc produces a library that is used by the
vim-vebugger for asynchrous execution.

Vim-vebugger uses its own leader key which is by default set to `<leader>d`.

**Few shortcuts**

`o`, **:VBGstepOver**

`c`, **:VBGcontinue**

`b`, **:VBGtoggleBreakpointThisLine**

`B`, **:VBGclearBreakpoints**

`e`, **:VBGevalWordUnderCursor** in normal mode, **:VBGevalSelectedText** in select mode

`x`, **:VBGexecute current line in normal mode.**

## [vim-session](https://github.com/xolox/vim-session)

Extended session management. Open sessions are autosaved every 5 minutes and
on session/vim close. No automatic session loading to avoid overwriting default.

**Few shortcuts**

`<leader>sw`: Save session (no argument for `default`)

`<leader>so`: Open session (prompt when multiple exist)

`<leader>sc`: Close current session

`<leader>sd`: Delete session (prompt when multiple exist)

## [vim-bbye](https://github.com/moll/vim-bbye)

Delete buffers without ruining your layout.

**Few shortcuts for easier management**

`<leader>bd`: Delete a buffer

`<leader>wd`: Close window

`<leader>ws`: Vertically split window

`<leader>wh`: Horizontally split window

`<leader>qq`: Close all windows and exit vim

## [deoplete](https://github.com/Shougo/deoplete.nvim)

Advanced completion framework. Around and buffer completion is disabled.

**Few shortcuts**

`<S-Tab>`, `<Up>`, `<C-p>`: Move up in the completion list.

`<Tab>`, `<Down>`, `<C-n>`: Move down in the completion list.

## [LanguageClient-neovim](https://github.com/autozimu/LanguageClient-neovim)

Provides completion and more, integrates with deoplete. Supports any language
through the language server protocol.

**Few shortcuts**

`<leader>lcc`: Open LanguageClient context menu.

`<leader>lco`: Open quickfix window containing LanguageClient diagnostics.

`<leader>lcd`: Close quickfix window containing LanguageClient diagnostics.

## [echodoc](https://github.com/Shougo/echodoc.vim)

Shows the function signature of the current function in the command line.
