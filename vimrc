"-------------------------------------------------
" Sections:
" Plugins
" General
" User interface
" Search
" Text (tab, spaces, indent)
" Deletion
" Code
"   Compiling
"   Style
"   Shortcuts
"   Tags
" Spell checking
" Latex
" Debugger
" Functions
"-------------------------------------------------


"-------------------------------------------------
" Plugins
" (Must be on top of the vimrc)
"-------------------------------------------------

" Automatically install vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/bundle')

" Plugins

" Enhanced hightlighting for C++ files.
Plug 'octol/vim-cpp-enhanced-highlight', { 'for': ['c', 'cpp'] }

" Auto aligning tables.
Plug 'dhruvasagar/vim-table-mode', { 'for': ['markdown', 'rst'] }

" Syntax checking on errors.
Plug 'scrooloose/syntastic'

" Explore the filesystem in vim.
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle'}

" Plantuml syntax.
Plug 'aklt/plantuml-syntax', { 'for': 'plantuml'}

" Latex live preview.
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex'}

" Browse tags of the current file.
Plug 'majutsushi/tagbar'

" Fuzzy file, buffer, mru, tag finder.
Plug 'ctrlpvim/ctrlp.vim'

" Statusline.
Plug 'vim-airline/vim-airline'

" Themes for airline.
Plug 'vim-airline/vim-airline-themes'

" Format code with clang-format
Plug 'rhysd/vim-clang-format', { 'for': ['c', 'cpp'] }

" Simplify Doxygen documentation, hosted in mirror due to broken git history.
Plug 'https://git.mel.vin/mirror/DoxygenToolkit.vim.git',
  \ { 'for': ['c', 'cpp'] }

" Asynchronous execution library (for vim-vebugger)
Plug 'Shougo/vimproc.vim', { 'do' : 'make', 'for': ['c', 'cpp'] }

" Debugger front-end for various languages
Plug 'idanarye/vim-vebugger', { 'for': ['c', 'cpp'] }

" Misc autoload vim scripts
Plug 'xolox/vim-misc'

" Extended session management
Plug 'xolox/vim-session'

" Delete buffers without ruining your layout.
Plug 'moll/vim-bbye'

" Completion framework.
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

" Language server for completion framework.
Plug 'autozimu/LanguageClient-neovim',
  \ { 'branch': 'next', 'do': 'make release', 'for': ['c', 'cpp'] }

" Show function signature in the command line.
Plug 'Shougo/echodoc.vim', { 'for': ['c', 'cpp'] }

" SELinux policy syntax.
Plug 'lzap/vim-selinux'

" All of the plugins must be added before the following line.
call plug#end()


"-------------------------------------------------
" General
"-------------------------------------------------

" Turn off vi compatibility.
set nocompatible

" Set the amount of lines of history to remember.
set history=100

" Set a mapleader key. This is used for extra key combinations.
let mapleader = '\'
map <Space> <leader>

" Better command line completion.
set wildmenu
set wildmode=list:longest,full

" Ignore compiled files.
set wildignore=*.o,*~,*.pyc

" Filetypes
autocmd BufRead,BufNewFile *.irst setfiletype rst
autocmd BufRead,BufNewFile *.puml setfiletype plantuml
autocmd BufRead,BufNewFile *.tikz setfiletype tex
autocmd BufRead,BufNewFile *.tpp setlocal filetype=cpp
" Avoid two language servers due to c and cpp being different filetypes.
" https://github.com/autozimu/LanguageClient-neovim/issues/536
autocmd BufRead,BufNewFile *.c setlocal filetype=cpp

" Extended session management.
let g:session_autoload='no'
let g:session_autosave='yes'
let g:session_autosave_periodic=5
let g:session_autosave_silent=1
let g:session_persist_font=0
let g:session_persist_colors=0
map <leader>sw :SaveSession
map <leader>so :OpenSession<cr>
map <leader>sc :CloseSession<cr>
map <leader>sd :DeleteSession


"-------------------------------------------------
" User interface
"-------------------------------------------------

" Display line numbers.
set number

" Set the line numbers relative from the current line.
set relativenumber

" Show the (partial) command in status line.
set showcmd

" Always display the status line, even if only one window is displayed.
set laststatus=2

" Shows a dialogue asking if the file has to be saved,
" instead of raising an error.
set confirm

" Keep the cursor in the centre of the buffer if possible.
set scrolloff=10000

" Toggle NERDTree.
map <C-t> :NERDTreeToggle<CR>
map <F3> :NERDTreeToggle<CR>

" Open a new tab with NERDTree.
map <leader>tn :tabnew ./<cr>

" Always show NERDTree on start if no files were specified.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() ==# 0 && !exists("s:std_in")
  \ | exe 'NERDTreeToggle' | endif

" Disable automatic folding.
set nofoldenable

" Add buffer switching with airline
let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
nmap <leader>bp <Plug>AirlineSelectPrevTab
nmap <leader>bn <Plug>AirlineSelectNextTab
nmap <S-Tab> <Plug>AirlineSelectPrevTab
nmap <Tab> <Plug>AirlineSelectNextTab

" Delete the current buffer without closing the windows
map <leader>bd :Bdelete<CR>
" Close window
map <leader>wd :q<CR>
" Vertical split
map <leader>ws :vsplit<CR>
" Horizontal split
map <leader>wh :split<CR>
" Exit vim
map <leader>qq :qa<CR>

" Easy vertical terminal split opening (why is this not built-in?)
cnoreabbrev vterm vert term


"-------------------------------------------------
" Colours and fonts
"-------------------------------------------------

" Enable Doxygen syntax highlighting.
let g:load_doxygen_syntax=1

" Set the colorscheme to bundled wombat256mod.
colorscheme wombat256mod

" Set the airline theme to molokai and enable, configure the tabline
let g:airline_theme='molokai'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamecollapse = 0

" Use 24-bit colours in the terminal, requires 'advanced' terminal emulator.
set termguicolors

" Make the terminal background transparant.
hi Normal guibg=NONE ctermbg=NONE

" Set encoding.
set encoding=utf8

" Set filetype.
set ffs=unix


"-------------------------------------------------
" Search
"-------------------------------------------------

" Highlights all matches of the search pattern.
set hlsearch

" Show all the matches while typing the search command.
set incsearch

" Ignores the case when searching.
set ignorecase

" Case insensitive search, except when capital letters are used.
set smartcase

" Redraw the screen when <C-L> is pressed.
" This also turns off the search highlighting until the next search.
nnoremap <C-L> :nohl<CR><C-L>

" Set the manual page section order for keywordprg
let $MANSECT="3p:3:2:1:n:l:8:0:5:4:9:6:7"

"-------------------------------------------------
" Text (tab, spaces, indent)
"-------------------------------------------------

" Tabsize of 8 (default).
set shiftwidth=8 tabstop=8

" Reset default behaviour to tabs instead of spaces for reStructuredText files.
autocmd Filetype rst setlocal noexpandtab softtabstop=8

" 2 spaces instead of a tab for YAML files.
autocmd Filetype bib,tex,yaml setlocal expandtab shiftwidth=2 softtabstop=2

" 4 spaces instead of a tab for Python and Haskell files.
autocmd Filetype python,haskell,lhaskell setlocal expandtab shiftwidth=4
  \ softtabstop=4

" Copy indent from current line when starting a new line.
set autoindent smartindent

" Highlight tabs and trailing spaces
set list listchars=tab:▸\ ,trail:·

" Removes trailing spaces when saving
autocmd BufWrite * :call Delete_trailing_spaces()

" Disable softwrapping on long lines
set nowrap

" Enable persistent undo if it is supported
if has('persistent_undo')
  let vundodir = expand('~/.vim/undo')
  if !isdirectory(vundodir)
    call mkdir(vundodir)
  endif
  let &undodir = vundodir
  set undofile
endif

" Use system clipboard
set clipboard=unnamedplus
autocmd FileType cpp set keywordprg=cppman


"-------------------------------------------------
" Deletion
"-------------------------------------------------

" Delete without putting the deleted words into the register.
map <leader>odw "_dw
map <leader>odW "_dW
map <leader>ode "_de
map <leader>odE "_dE
map <leader>odb "_db
map <leader>odB "_dB
map <leader>odd "_dd
map <leader>od^ "_d^
map <leader>od$ "_d$
map <leader>od{ "_d{
map <leader>od} "_d}
map <leader>odi( "_di(
map <leader>odi) "_di)
map <leader>odi' "_di'
map <leader>odi" "_di"
map <leader>odi< "_di<
map <leader>odi> "_di>
map <leader>odf. "_df.
map <leader>odf? "_df?
map <leader>odf! "_df!
map <leader>odt. "_dt.
map <leader>odt? "_dt?
map <leader>odt! "_dt!


"-------------------------------------------------
" Code
" Compiling
"-------------------------------------------------

" Show warnings.
set statusline=%#warningmsg#%{SyntasticStatuslineFlag()}%*

" Set syntastic options.
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_wq = 0
" Compiles the current file.
map <leader>c :SyntasticCheck<cr>
" Opens the location list that shows the errors.
map <leader>co :lopen<cr><C-w><C-p>
" Closes the location list that shows the errors.
map <leader>cd :lclose<cr>

" Close any preview window that is open
map <leader>cf :pclose<cr>

" Set vim-clang-format options.
let g:clang_format#detect_style_file = 1
let g:clang_format#auto_format = 1
let g:clang_format#auto_format_on_insert_leave = 0 " breaks DoxygenToolkit.vim
let g:clang_format#enable_fallback_style = 0

" Completion framework.
let g:deoplete#enable_at_startup = 1
" Disable completion sources around and buffer.
call deoplete#custom#option('ignore_sources', {'_': ['around', 'buffer']})
" Disable abbreviation of signatures (also causes issues with echodoc)
call deoplete#custom#source('_', 'max_abbr_width', 0)
" Disable preview window containing function documentation.
set completeopt-=preview
" Map arrow keys to <C-p> and <C-n> for better selection behaviour.
:inoremap <expr><Up> pumvisible() ? "\<C-p>" : "\<Up>"
:inoremap <expr><Down> pumvisible() ? "\<C-n>" : "\<Down>"
" Allow selection of completion with tab and shift+tab.
:inoremap <expr><S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
:inoremap <expr><Tab> pumvisible() ? "\<C-n>" : "\<Tab>"

" Language server for completion framework.
" Disable autostart for C/C++, see Compilation_database_build_dir_set().
let g:LanguageClient_autoStart = 0
" Disable diagnostics showing in the SignColumn (gutter) and highlight
" as it is too intrusive and likely conflicts with syntastic.
let g:LanguageClient_diagnosticsSignsMax = 0

" Show function signatures in the command line.
let g:echodoc#enable_at_startup = 1
" Disable showing of mode in the command line, avoids conflict with echodoc.
set noshowmode

" C specific.
let g:syntastic_c_checkers = ['clang_tidy']
let g:syntastic_c_clang_tidy_post_args = ''
autocmd BufEnter * if &filetype ==# 'c'
  \ | :call Compilation_database_build_dir_set() | endif

" C++ specific.
let g:syntastic_cpp_checkers = ['clang_tidy']
let g:syntastic_cpp_clang_tidy_post_args = ''
autocmd BufEnter * if &filetype ==# 'cpp'
  \ | :call Compilation_database_build_dir_set() | endif

" Doxygen
let g:DoxygenToolkit_briefTag_pre = ''
let g:DoxygenToolkit_templateParamTag_pre = '\tparam '
let g:DoxygenToolkit_templateParamTag_post = ' '
let g:DoxygenToolkit_paramTag_pre = '\param '
let g:DoxygenToolkit_paramTag_post = ' '
let g:DoxygenToolkit_returnTag = '\return '
let g:DoxygenToolkit_throwTag_pre = '\throw '
let g:DoxygenToolkit_throwTag_post = ' '
let g:DoxygenToolkit_fileTag = '\file '
let g:DoxygenToolkit_authorTag = '\author '
let g:DoxygenToolkit_dateTag = '\date '
let g:DoxygenToolkit_versionTag = '\version '
let g:DoxygenToolkit_blockTag = '\name '
let g:DoxygenToolkit_classTag = '\class '
" C is /** */, C++ is ///
let g:DoxygenToolkit_commentType = 'C'
" allow /** \brief Foo */ for e.g. enum doc.
let g:DoxygenToolkit_compactOneLineDoc = 'yes'
" No empty line between e.g. brief and param section.
let g:DoxygenToolkit_compactDoc = 'yes'

" use <leader>ENTER to generate Doxygen block
map <leader><cr> :Dox<cr>


"-------------------------------------------------
" Code
" Style
"-------------------------------------------------

" Tip: format paragraph with 'gq' in normal mode.
" Line wrap: default to 80 chars, except for python.
set tw=80
autocmd Filetype python setlocal tw=79
" Line wrap: recognise list alignment.
set fo+=n
" Line wrap: remove comment leader when joining lines.
set fo+=j

" Highlight the first 3 characters over 80 character limit.
autocmd BufEnter * highlight OverLength ctermbg=darkgrey guibg=#501010
autocmd BufEnter * match OverLength '\%<84v.\%>81v'

" Set colour for the vertical line that shows the character limit.
highlight ColorColumn ctermbg=Grey guibg=#232526

" Toggle between the vertical line and the highlighting of characters.
map <leader>cl :call Colorcolumn_highlighting()<cr>

" Disable checks for RST to avoid errors at unknown directives.
let g:syntastic_rst_checkers = []

" Set table mode settings for RST.
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='


"-------------------------------------------------
" Code
" Shortcuts
"-------------------------------------------------

" Map combination for lc context menu
nnoremap <leader>lcc :call LanguageClient_contextMenu()<cr>
" Open the quickfix window containing lc diagnostics
map <leader>lco :copen<cr><C-w><C-p>
" Close the quickfix window containing lc diagnostics
map <leader>lcd :cclose<cr>

"-------------------------------------------------
" Code
" Tags
"-------------------------------------------------

map <leader>t :TagbarToggle<cr>


"-------------------------------------------------
" Spell checking
"-------------------------------------------------

" Set spell checking.
set spell spelllang=en_gb

" Disable spell check for some problematic filetypes.
autocmd Filetype diff,gitrebase,plantuml,te,yaml setlocal nospell

" Toggle spell checking.
map <leader>ss :setlocal spell!<cr>
map <leader>sl :call Spellcheck_cycle_lang()<cr>

" Shortcuts for spell checking.
map <leader>sn ]s " Next misspelled word.
map <leader>sp [s " Previous mispelled word.
map <leader>sa zg " Add word to dictionary.
map <leader>s? z= " List alternative words.

" Set underline instead of highlighting of misspelled words.
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red
hi clear SpellCap
hi SpellCap cterm=underline ctermfg=grey
hi clear SpellLocal
hi SpellLocal cterm=underline ctermfg=lightblue
hi clear SpellRare
hi SpellRare cterm=underline ctermfg=darkblue


"-------------------------------------------------
" Latex
"-------------------------------------------------

" Use only lacheck to check files.
let g:syntastic_tex_checkers = ['lacheck']

" important: grep will sometimes skip displaying the file name if you
" search in a singe file. This will confuse Latex-Suite. Set your grep
" program to always generate a file name.
set grepprg=grep\ -nH\ $*

" optional: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

" optional: Set the compiler output to pdf instead of dvi
let g:Tex_DefaultTargetFormat='pdf'

" first let vim compile then the references and then two times pdf again to
" get the references straight
let g:Tex_MultipleCompileFormats='pdf,bib,pdf,pdf'

" the PDF viewer for normal viewing.
let g:Tex_ViewRule_pdf='mupdf'

" the PDF viewer for live previewing. (Not all PDF viewers are supported)
let g:livepreview_previewer = 'okular'

" set the refreshing time of live previewing.
set updatetime=1000

" start live previewing
map <leader>lo :LLPStartPreview<cr>


"-------------------------------------------------
" Debugger
"-------------------------------------------------

" Set the leader key for shortcuts
let g:vebugger_leader = '<leader>d'


"-------------------------------------------------
" Functions
"-------------------------------------------------

" Toggle between the vertical line and the highlighting of characters.
func! Colorcolumn_highlighting()
  if &l:colorcolumn ==# 81
    setlocal colorcolumn&
    match OverLength '\%<84v.\%>81v'
  else
    setlocal colorcolumn=81
    match OverLength /\%1000v.\+/
  endif
endfunc

func! Compilation_database_langserv_start(timer)
  LanguageClientStart
endfunc

" Finds and set the dir containing C/C++ compilation database
func! Compilation_database_build_dir_set()
  let l:db_pre = expand('%:p:h')
  let l:db_post = ''
  while !filereadable(l:db_pre . l:db_post . '/compile_commands.json')
    " probe a potential build dir
    if filereadable(l:db_pre . l:db_post . '/build/compile_commands.json')
      let l:db_post = l:db_post . '/build'
      break
    endif
    " otherwise try a directory up
    let l:db_post = l:db_post . '/..'
    " Give up after after 10 dirs up (5 + 3 * 10).
    if strlen(l:db_post) > 35
      let l:db_pre = ''
      let l:db_post = ''
      break
    endif
  endwhile

  " Simplify the dir path, changing /dir/src/../build to /dir/build
  let l:db_dir = simplify(l:db_pre . l:db_post)

  " Stop here if path hasn't changed to avoid language server restart.
  if exists('s:compdb_dir') &&
    \ s:compdb_dir ==# l:db_dir
    return 0
  endif
  let s:compdb_dir = l:db_dir

  " Syntastic.
  let g:syntastic_c_clang_tidy_args = '-p=''' . s:compdb_dir . ''''
  let g:syntastic_cpp_clang_tidy_args = g:syntastic_c_clang_tidy_args

  " Language server for completion framework.
  LanguageClientStop
  let g:LanguageClient_serverCommands = {
    \ 'c': ['clangd', '-compile-commands-dir=' . s:compdb_dir],
    \ 'cpp': ['clangd', '-compile-commands-dir=' . s:compdb_dir]
  \ }

  if !exists('s:compdb_langserv_firststart')
    " The first time we are starting we do it instantly. This avoids an issue
    " when restoring a session that has a different filetype as the active
    " buffer. By the time the timer would be done the active buffer is changed
    " causing LanguageClientStart to error because there is no server associated
    " with the buffer's filetype.
    let s:compdb_langserv_firststart = 1
    LanguageClientStart
  else
    " Calling start immediately after stop does not work, so set a timer. If the
    " user switches buffer again within the timer duration this may fail due to
    " potential filetype change, see above note.
    let l:timer = timer_start(1000, 'Compilation_database_langserv_start')
  endif
endfunc

" Removes trailing spaces when saving
" http://amix.dk/vim/vimrc.html
func! Delete_trailing_spaces()
  " Do not execute for diff (patch) files, spaces are part of the context.
  if &filetype ==# 'diff' || &filetype ==# 'markdown'
    return 0
  endif
  exe "normal mz"
    %s/\s\+$//ge
  exe "normal `z"
endfunc

" Cycle between spellcheck languages
func! Spellcheck_cycle_lang()
  " If spellchecking is disabled, just enable it only
  if &spell ==# 0
    setlocal spell!
    echo 'enabled spell checking'
    return 0
  endif

  if &spelllang ==# 'en_gb'
    let l:lang = 'nl'
  else
    let l:lang = 'en_gb'
  endif
  let &spelllang = l:lang
  echo 'changed spell checking language to ' . l:lang
endfunc
